#!/bin/bash

cat <<EOF | lxd init --preseed
config:
  core.https_address: '[::]:8443'
  core.trust_password: lxd-in-docker
networks:
# - config:
#     ipv4.address: auto
#     ipv6.address: none
#   description: ""
#   managed: false
#   name: lxdbr0
#   type: ""
storage_pools:
- config:
    source: /storage_pool_lxd
  description: ""
  name: lxd-in-docker-pool
  driver: dir
profiles:
- config: {}
  description: ""
  devices:
    eth0:
      nictype: macvlan
      parent: eth0
      type: nic
      name: eth0
    root:
      path: /
      pool: lxd-in-docker-pool
      type: disk
  name: default
cluster: null
EOF

lxc profile set default raw.lxc=lxc.apparmor.allow_incomplete=1
lxc profile set default security.privileged true
lxc profile set default raw.lxc=lxc.apparmor.profile=unconfined
