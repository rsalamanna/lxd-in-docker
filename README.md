# README #

First of all, let me send huge kudos to the LXD developer community. I love it.
With that out of the way: this is a hack to run LXD inside docker. It downloads the version you specify and then compiles builds etc etc.
BIG DISCLAIMER! For the love of all that is holy, DO NOT use this to run serious stuff in production. I had to solve a problem that probably nobody has, but just in case anybody googles "lxd in docker", here's a way to accomplish that.

### Noteworthy ###

* Default version LXD 3.20
* But feel free to build your own! You can specify any LXD version you want, check the Dockerfile for the variable name
* If instead you just want the image, docker pull away my friend (rsaruman86/lxd-in-docker)
* I would take good care of mapping some volumes to the /go directory and for the lxd storage pool
* I created a config script to preseed a simple configuration for a new container, it uses macvlan as default network and it includes the apparmor disable. It's in the /go folder, it's simple and fast. It works for me cause I use this as a throwaway lab.
* The docker container must run as privileged. Example"
```
docker run --name testing -id --hostname=lxd-in-docker --privileged --cap-add=ALL -v /dev:/dev -v /lib/modules:/lib/modules -v /sys/fs/cgroup:/sys/fs/cgroup:rw lxd-in-docker:3.20
```
* Please, if you think this readme sucks, help me make it better :)
