FROM ubuntu:18.04

ARG LXD_VERSION='lxd-3.20'
ARG GOINSTALL='/go'

ENV CGO_CFLAGS="-I$GOINSTALL/lxd/_dist/deps/sqlite/ -I$GOINSTALL/lxd/_dist/deps/libco/ -I$GOINSTALL/lxd/_dist/deps/raft/include/ -I$GOINSTALL/lxd/_dist/deps/dqlite/include/"
ENV CGO_LDFLAGS="-L$GOINSTALL/lxd/_dist/deps/sqlite/.libs/ -L$GOINSTALL/lxd/_dist/deps/libco/ -L$GOINSTALL/lxd/_dist/deps/raft/.libs -L$GOINSTALL/lxd/_dist/deps/dqlite/.libs/"
ENV LD_LIBRARY_PATH="$GOINSTALL/lxd/_dist/deps/sqlite/.libs/:$GOINSTALL/lxd/_dist/deps/libco/:$GOINSTALL/lxd/_dist/deps/raft/.libs/:$GOINSTALL/lxd/_dist/deps/dqlite/.libs/"
ENV CGO_LDFLAGS_ALLOW="-Wl,-wrap,pthread_create"
ENV GOPATH=$GOINSTALL/lxd/_dist
ENV GOBIN=$GOPATH/bin

RUN apt-get update && apt install -y wget

RUN export DEBIAN_FRONTEND=noninteractive && \
  ln -fs /usr/share/zoneinfo/Europe/Madrid /etc/localtime && \
  apt-get update && \
  apt-get install -y tzdata software-properties-common && \
  dpkg-reconfigure --frontend noninteractive tzdata && \
  add-apt-repository ppa:longsleep/golang-backports -y && \
  apt-get install -y bash-completion* acl autoconf dnsmasq-base git golang-go golang libacl1-dev libcap-dev liblxc1 liblxc-dev libtool libudev-dev libuv1-dev make pkg-config rsync squashfs-tools tar tcl xz-utils ebtables libapparmor-dev libseccomp-dev libcap-dev btrfs-tools curl wget htop nano tree cgroup* ubuntu-minimal net-tools && \
  apt clean && apt autoremove -y && \
  mkdir -p $GOINSTALL && \
  cd $GOINSTALL && \
  wget https://github.com/lxc/lxd/releases/download/$LXD_VERSION/$LXD_VERSION.tar.gz && \
  tar xvf $LXD_VERSION.tar.gz && \
  ln -s $LXD_VERSION lxd && cd lxd && \
  make deps && \
  make

RUN echo "export GOPATH='$GOPATH' \n\
export GOBIN='$GOBIN' \n\
export PATH=\$PATH:$GOBIN \n\
export LD_LIBRARY_PATH=${GOPATH}/deps/sqlite/.libs/:${GOPATH}/deps/dqlite/.libs/:${GOPATH}/deps/raft/.libs:${GOPATH}/deps/libco/:${LD_LIBRARY_PATH} \n"\
>> /root/.bashrc && \
echo "root:1000000:65536" | tee -a /etc/subuid /etc/subgid && \
ln -s /usr/share/bash-completion/completions/lxd.lxc /usr/share/bash-completion/completions/lxc

COPY lxd-in-docker-preseed.sh /go/lxd-in-docker-preseed.sh
COPY lxd-completion.sh  /usr/share/bash-completion/completions/lxd.lxc

CMD ["/go/lxd/_dist/bin/lxd"]
